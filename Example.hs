module Main where

import RobSock.RobSockMonad
import RobSock.RobSockUtils

import Control.Monad
import Control.Monad.State
import Control.Monad.Trans
import Control.Applicative
import Data.Maybe(fromJust)
import System.Environment
import GHC.Float

import qualified Traversal.Movement as M
import Traversal.GreedyTraversal
import Traversal.Map
import Points.Point

import Debug.Trace

aa = []

main :: IO ()
main = do [name,pos,host] <- getArgs
          (x,_) <- runStateT (start name (read pos) host) NoMouse
          case x of
            (-1) -> putStrLn "Failed to connect"
            _    -> putStrLn "Connection finished"

start :: String -> Int -> String -> MM Int
start n p h = do get >>= lift.putStrLn.show
                 x <- initRobot n p h 
                 case x of
                   (-1) -> return $ -1
                   y    -> waitStart >> return 0

waitStart:: MM ()
waitStart = do get -- >>= lift.putStrLn.show 
               readSensors
               getLSpeed -- >>= \l -> getRSpeed 
                         -- >>= lift.putStrLn.(("Speed:" ++ show l) ++).show
               x <- getStartButton 
               case x of
                 True  -> movingToCheese 0
                 False -> waitStart

movingToCheese :: Int -> MM ()
movingToCheese c = do get -- >>= lift.putStrLn.show  
                      x <- fetchMessages -- >>= lift.putStrLn.show
                      readSensors -- >>= lift.putStrLn.show
                      -- lift.putStrLn $ "Counter:"++(show c)
                      sensors <- getSensors
                      -- lift.putStrLn.show $ sensors
                      if (c == 30) 
                         then runWith (target (floc sensors)) M.MRight emp
                         else driveMotors 0 0
                      nextSensorsReq c
                      movingToCheese (c+1)
                   where target s = 
                                case s of
                                     Nothing -> (0,0)
                                     (Just (sx,sy)) -> (sx+3,sy-6)

runWith :: Point -> M.Movement -> TravMap -> MM ()
runWith p m t =
        do s <- retGoodGps
           trace ((show s)++","++(show p)) $ tryTraversal s
       where 
             tryTraversal p' = 
                          case (traversal (scale p') (scale p) t) of
                          --     Nothing -> driveMotors 0 0 
                          --     (Just movs) -> newKnowledge movs p'
                          _ -> newKnowledge M.test_1 p'
             newKnowledge movs a = 
                          do x <- (followPath a m movs t)
                             case x of
                                  Left _ -> driveMotors 0 0
                                  Right (m,t') -> trace (show t') $
                                                        runWith p m t'
       
type PathRes = Either () (M.Movement,TravMap)

followPath :: Point -> M.Movement -> [M.Movement] -> TravMap -> MM (PathRes)
followPath p m [] t =  driveMotors 0 0 >> (return $ Left ())
followPath p m (x:xs) t = do
           checkObs <- checkForObstacles p t 
           case checkObs of
                _ -> mkMove m x
              --  Nothing -> mkMove m x  
               -- (Just t') -> return $ Right (m,t')
           where
                mkMove bef cur = 
                      let (a,b) = M.determineDirection (m,x) in
                          if (M.isDiag x)
                             then rotate ((a*pi)/4) b >> moveD 
                                  >> followPath p x xs t
                             else rotate ((a*pi)/4) b >> move8 
                                  >> followPath p x xs t                         
      
checkForObstacles :: Point -> TravMap -> MM (Maybe TravMap)
checkForObstacles p t = do 
                  s <- procSensors
                  --trace (pr (obsLeft s, obsCenter s, obsRight s)) 
                  return $ updateMap p (l s) t
                  where 
                    l s = [(obsLeft s,SLeft),(obsCenter s,SCenter),(obsRight s, SRight)]
                    pr (a,b,c) = (show a)++"->"++(show SLeft)++";   "++
                                 (show b)++"->"++(show SCenter)++";  "++
                                 (show c)++"->"++(show SRight)    

-------------------------------------------------------------------------------
-- * Auxiliary functions

move :: MM ()
move =  driveMotors 0.1 0.1 >> readSensors >>
        driveMotors 0.05 0.05 >> readSensors >>
        driveMotors (-0.05) (-0.05)  

move4 = move >> move >> move >> move
move8 = move4 >> move4

moveS :: MM ()
moveS = driveMotors 0.05 0.05 >> readSensors >>
        driveMotors 0.025 0.025 >> readSensors >>
        driveMotors 0.025 0.025 >> readSensors >>
        driveMotors 0.025 0.025 >> readSensors >>
        driveMotors (-0.025) (-0.025)

moveD :: MM ()
moveD = driveMotors 0.1 0.1 >> readSensors >>
       driveMotors 0.05 0.05 >> readSensors >>
       driveMotors (-0.0293) (-0.0293) >> readSensors >>
       driveMotors 0.0293 0.0293

-- utility functions regarding the location sensor

floc :: Sensors -> Maybe (Float,Float)
floc s = case (gps s) of
     Nothing -> Nothing
     Just (a,b) -> Just (double2Float a, double2Float b)

scale :: (Float, Float) -> (Float, Float)
scale (a,b) = (a*10,b*10)

retGoodGps :: MM Point
retGoodGps = (rec 0 (return [])) >>= (return . (\x -> g $ foldr f (0,0) x))
       where f (a,b) (c,d) = (a+c,b+d)
             g (a,b) = (roundTo (a/10) 1, roundTo (b/10) 1) 
             roundTo = (\x y -> (fromInteger $ round $ x * (10^y)) / (10.0^^y)) 
 
procSensors :: MM Sensors
procSensors = do
            nextSensorsReq 0
            readSensors
            getSensors

rec :: Int -> MM [Point] -> MM [Point]
rec cnt l = 
    if cnt < 10
       then do s <- procSensors
               case (floc s) of
                    Nothing -> rec cnt l
                    (Just x) -> do l' <- rec (cnt+1) l
                                   return (x:l')
       else l

