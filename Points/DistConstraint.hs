{- 
   Description : This file regards the definition of distance constraint.
   It also provides some functions for calculating points that meet a given constraint.
   Author : Renato Neves, Cláudio Lourenço
-}

module Points.DistConstraint where

import Data.Set

-- imports the machinery regarding euclidean points
import Points.Point                         

-- the definition of distance constraint in the context of our project
type DistConstraint = (Float,Point)

{- returns a set of points that meet the given constraint. Such points are calculated
through the equations x = cost t and y = sin t, with t denoting an angle in degrees.
-}
acceptablePoints :: DistConstraint -> Set Point
acceptablePoints (r,p) = buildSetPoints 0 empty
                 where buildSetPoints d ps =
                                      if d > 355 then ps
                                      else buildSetPoints (d+5) newps
                                      where p' = circleValue p r d
                                            newps = insert p' ps


accepPointsAngle :: DistConstraint -> Float -> Float -> Set Point
accepPointsAngle (r,p) b e = buildSetPoints b empty
                      where buildSetPoints d ps =
                                           if d > e then ps
                                           else buildSetPoints (d+5) newps
                                           where p' = circleValue p r d
                                                 newps = insert p' ps

-- a value of the circle
-- :: Center -> Radius -> Angle (degrees) -> resulting Point
circleValue :: Point -> Float -> Float -> Point
circleValue (cx,cy) r d = (x,y)
            where                   
                  x = cx + r * (cos (rad d))
                  y = cy + r * (sin (rad d))

-- a set of distance constraints
type DistContraints = Set DistConstraint

-- from degrees to radians
rad :: Float -> Float
rad d = d*(pi/180)