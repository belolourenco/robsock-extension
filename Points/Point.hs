{- 
   Description : This file regards the definition of euclidean point, along with 
   useful functions over this sort of points.
   Author : Renato Neves, Cláudio Lourenço
-}

module Points.Point where

import Data.Set 
import qualified Data.List as L

import Control.Applicative

-- the definition of euclidean point
type Point = (Float,Float)

-- the distance between two euclidean points
distance :: Point -> Point -> Float
distance a  b = sqrt $ dx^2 + dy^2 
         where  absDiff = abs . (uncurry (-))
                dx = absDiff (fst a, fst b)
                dy = absDiff (snd a, snd b)

-- given two sets of points, returns the two points (one from each set)
-- that have the shortest distance between themselves
closPointSet :: Set Point -> Set Point -> (Point,Point)
closPointSet as bs = Data.Set.foldr f init as
            where get1 = head . toList
                  oneA = get1 as
                  init = (oneA, closPoint oneA bs)
                  f a init =  
                    let closP = closPoint a bs in
                        if distance a closP < (uncurry distance) init
                           then (a, closP) 
                           else init

-- given a point and a set of points, returns the point from the set
-- that is closest to the former.
closPoint :: Point -> Set Point -> Point
closPoint a bs = Data.Set.foldr f (get1 bs) bs   
            where get1 = head . toList
                  f p init = 
                    if (distance a p) < (distance a init) 
                       then p
                       else init    

-- given two points returns the middle point               
middlePoint :: Point -> Point -> Point
middlePoint a b = ((x+x')/2,(y+y')/2)
            where x = fst a
                  x' = fst b
                  y = snd a
                  y' = snd b 
            
-- returns the neighbors of a given point 
neigh :: Point -> [Point]
neigh (x,y) = (fmap (\a b -> (a,b))  lx) <*> ly
      where lx = [(+1),(+ (-0.5))] <*> pure x
            ly = [(+ (-0.5)),(+0.5)] <*> pure y          

roundNeigh :: [Point] -> [Point]
roundNeigh = fmap ((bimap fromIntegral).r)
           where r (x,y) = (floor x, ceiling y)

-- aux function
bimap :: (a -> b) -> (a,a) -> (b,b)
bimap = \f (a,b) -> (f a, f b)