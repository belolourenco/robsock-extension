{- 
   Description : This file takes formations as graphs, and gives some useful functions
   Author : Renato Neves, Cláudio Lourenço
-}

module FormGraph where

-- imports the definition of agent
import Agent
import Data.Set


-- the definition of formation
type Formation = Set (Agent,(Float,Agent))


-- gives all the labeled edges coming from the given agent
edgesFromAgent :: Agent -> Formation -> Set (Float,Agent)
edgesFromAgent a form = Data.Set.foldr f empty form
               where f x y = if fst x == a then insert (snd x) y else y
