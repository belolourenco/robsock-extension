{- 
   Description : This file defines a greedy traversal.
   Author : Renato Neves, Cláudio Lourenço
-}

module Traversal.GreedyTraversal where

-- imports the possible movements of an agent
import qualified Traversal.Movement as M

-- imports the definition of map
import Traversal.Map

-- imports the definition of point
import Points.Point

import Data.Foldable
import Data.List 
import qualified Data.Map as DM
import System.Process(runCommand)

-- the point itself and its father. Also the distance to the end
type Position = ((Point, Point), Float)

-- this function returns the set of movements that an agent
-- must make in order to reach a given point (2nd argument)
-- by starting in another given point (1st argument) and by taking
-- the terrain (3rd argument) into account.
traversal :: Point -> Point -> TravMap -> Maybe [M.Movement]
traversal start end m = 
           traversal_rec start end [((start,start),0)] [] m

traversal_rec :: Point -> Point -> [ Position ] -> [ Position  ]-> 
               TravMap -> Maybe [M.Movement] 
traversal_rec _ _ [] _ _ = Nothing 
traversal_rec start end open closed m = 
            if (fst . fst) best == end
               then Just res 
               else 
                  let expanded = expand $ (fst . fst) best
                      expanded' = filter filterF expanded
                      expanded'' = addFath ((fst . fst) best) expanded'
                      expanded''' = addDist end expanded''
                      newOpen = foldr' (insertBy ord') rest expanded''' 
                      in 
                      traversal_rec start end newOpen newClosed m
               where 
                     best = head open
                     rest = tail open 
                     newClosed = insert best closed
                     filterF a = 
                             (not . isBlocked m) a && 
                             (not . isClosed closed) a &&
                             (not . isOpen open) a
                     ord' a b = compare (snd a) (snd b)
                     res = mkPath start end newClosed
                     
-- choses the point closest to the end
heur :: Point -> Point -> Point -> Point -> Ordering
heur end start a b 
     | distance a end + distance a start <= 
       distance b end + distance b start = LT
     | otherwise = GT

-- makes a path from a traversal
mkPath :: Point -> Point -> [ Position ] -> [M.Movement]
mkPath start end sp = mkPath_rec start end sp []

mkPath_rec :: Point -> Point -> [ Position ] -> [M.Movement] ->
            [M.Movement]
mkPath_rec start end sp movs = 
       if start == end then movs else
          case match of 
               Nothing -> undefined
               Just x@((p,fath),_) ->  
                    mkPath_rec start fath (delete x sp) (newMovs p fath)
       where match = Data.List.find (\p -> (fst . fst) p == end) sp
             newMovs (a,b) (a',b') 
                     | a'+1 == a && b'+1 == b = M.MUpRight : movs
                     | a'-1 == a && b'+1 == b = M.MUpLeft : movs
                     | a'+1 == a && b'-1 == b = M.MDownRight : movs
                     | a'-1 == a && b'-1 == b = M.MDownLeft : movs
                     | a'+1 == a && b' == b = M.MRight : movs
                     | a'-1 == a && b' == b = M.MLeft : movs
                     | a' == a && b'+1 == b = M.MUp : movs
                     | a' == a && b'-1 == b = M.MDown : movs  

expand :: Point -> [ Point ]
expand (x,y) = 
       insert (x-1,y-1) $
       insert (x+1,y-1) $
       insert (x-1,y+1) $
       insert (x+1,y+1) $
       insert (x,y-1) $
       insert (x,y+1) $
       insert (x-1,y) $
       insert (x+1,y) []       

isBlocked :: TravMap -> Point -> Bool 
isBlocked m p = case (DM.lookup p m) of
           Nothing -> False
           Just Free -> False
           otherwise -> True


isClosed :: [ Position ] -> Point -> Bool
isClosed sp p = Prelude.any (\x -> (fst . fst) x == p) sp

isOpen :: [Position] -> Point -> Bool
isOpen ls p = Prelude.any (\x -> (fst . fst) x == p) ls

addFath :: Point -> [ Point ] -> [ (Point,Point) ]
addFath p s = fmap (\a -> (a,p)) s

addDist :: Point -> [ (Point,Point) ] -> [ Position ]
addDist p s = fmap (\a -> (a, distance (fst a) p)) s  

-------------------- Utilities for testing

data Trace = Start | End | Visited
type TraceMap = DM.Map Point Trace

-- makes a map showing the "footprints" of the robot
fillTraceMap :: Point -> Point -> [M.Movement] -> TraceMap
fillTraceMap start end movs = 
             ins start Start $
             snd $
             Prelude.foldl f (start,initMap) movs
              where 
                    initMap = ins end End DM.empty
                    ins = DM.insert
                    insVisPoint = \(x,y) m -> ins (x,y) Visited m  
                    f ((x,y),b) a = case a of
                      M.MUp -> ((x,y+1), insXY) 
                      M.MDown -> ((x,y-1), insXY)
                      M.MLeft -> ((x-1,y), insXY) 
                      M.MRight -> ((x+1,y), insXY)
                      M.MUpLeft -> ((x-1,y+1), insXY)
                      M.MUpRight -> ((x+1,y+1), insXY)
                      M.MDownLeft -> ((x-1,y-1), insXY)
                      M.MDownRight -> ((x+1,y-1), insXY)
                      where insXY = insVisPoint (x,y) b

-- | Returns a representation of the map
pretty_ :: Int -> TravMap -> TraceMap -> String
pretty_ n m m'= pretty_rec (-n) ""
       where 
       pretty_rec y s = if y <= n
                  then pretty_rec (y+1) $ (prettyLine (-n) y "") ++ s
                  else addBreak s  
       prettyLine x y s = if x <= n 
                  then prettyLine (x+1) y $ s ++ (addSymbol (x, y))
                  else addBreak s 
      
       addSymbol = addSpace . symbol . mkPoint
       mkPoint (a,b) = (fromIntegral a, fromIntegral b) 
       symbol p = case (DM.lookup p m') of
              Just Start -> "S"
              Just End -> "E"
              Just Visited -> "i"
              Nothing -> case (DM.lookup p m) of
                     Nothing -> "-"
                     Just Blocked -> "X"
                     Just Free -> " "
       addSpace s = s++" "
       addBreak s = s++"\n"

printPNM :: Int -> String -> IO ()
printPNM n s = (writeFile "map.ppm" $ generatePNM header s)
               >> runCommand "pnmtopng map.ppm > map.png" >> return ()
  where header = "P3\n"++(show $ n*2+1)++" "++(show $ n*2+1)++"\n15\n"
        generatePNM header = Data.List.foldl f header 
        
        f a 'S' = a ++ "7 7 7"
        f a 'E' = a ++ "3 3 3"
        f a 'i' = a ++ "7 7 7"
        f a '-' = a ++ "15 15 15"
        f a 'X' = a ++ "0 0 0"
        f a x   = a ++ [x]

-- pretty prints a traversal 
pretty :: Int -> TravMap -> TraceMap -> IO ()
pretty n m m'= putStr $ pretty_ n m m'

pretty' :: Int -> Maybe TravMap -> IO ()
pretty' n m = 
        case m of 
             Nothing -> putStr "No map"
             (Just x) -> pretty n x DM.empty


traversesAndPrintsInFile :: Point -> Point -> TravMap -> IO ()
traversesAndPrintsInFile start end m = 
                   let tr = traversal start end m in
                       case tr of
                            Nothing -> putStr "No path ?!\n"
                            Just x -> printPNM 10 $ pretty_  10 m 
                                 $ fillTraceMap start end x  

traversesAndPrints :: Point -> Point -> TravMap -> IO ()
traversesAndPrints start end m = 
                   let tr = traversal start end m in
                       case tr of
                            Nothing -> putStr "No path ?!\n"
                            Just x -> pretty 10 m 
                                 $ fillTraceMap start end x  
-- pretty prints a traversal 
showmap :: Float -> TravMap -> Point -> String
showmap n m p = pretty_rec ((snd p)-n) ""
       where 
       pretty_rec y s = if y <= ((snd p)+n)
                  then pretty_rec (y+1) $ 
                                  (prettyLine ((fst p)-n) y "") ++ s
                  else addBreak s  
       prettyLine x y s = if x <= ((fst p)+n) 
                  then prettyLine (x+1) y $ s ++ 
                       (addSymbol (x+(fst p), y+(snd p)))
                  else addBreak s 
      
       addSymbol = addSpace . symbol -- . mkPoint
       --mkPoint (a,b) = (fromIntegral a, fromIntegral b) 
       symbol p = case (DM.lookup p m) of
                     Nothing -> "-"
                     Just Blocked -> "X"
                     Just Free -> " "
       addSpace s = s++" "
       addBreak s = s++"\n"

-- prettyUpdate = y x 
--              where x = updateMap (0,-9) 
--                      [] DM.empty
--                    y a = case a of 
--                      Nothing -> pretty 10 DM.empty DM.empty
--                      Just x' -> pretty 10 x' DM.empty
