{- 
   Description : This file defines a map.
   Author : Renato Neves, Cláudio Lourenço
-}

module Traversal.Map where

import Data.Map
import qualified Data.Set as S
import Points.Point
import Points.DistConstraint

import Debug.Trace

-- the cost of a "square"
data Cost = Free | Blocked deriving Show

-- A map is a mapping between points and costs.
-- We don't know the cost of a point that is not mapped.
type TravMap = Map Point Cost 

addBlocks :: TravMap -> [Point] -> TravMap
addBlocks = Prelude.foldr (\a b -> insert a Blocked b)

-- gives the points covered by the sensor given restrictions
-- regarding x and y coordinates (1st,2nd args)
areaOfSensor :: Float -> Float -> [Point] -> [Point]
areaOfSensor rx ry = Prelude.filter (\(a,b) -> a <= rx && b >= ry)


-- the positions of obstacle sensors
data Sens = SLeft | SRight | SCenter 
     deriving Show

-- given a point and the values of the obstacle sensors, updates
-- the map 
updateMap :: Point -> [(Maybe Double, Sens)] -> TravMap -> Maybe TravMap
updateMap (x,y) l m = let m' = Prelude.foldr f m l in
          if (length . keys) m /= (length . keys) m' 
             then Just m' else Nothing
   
         where f (Nothing, _) b = b
               f (Just a,a') b = case a' of
                 SLeft -> update a 150 b a'
                 SRight -> update a 30 b a'
                 SCenter -> update a 90 b a'
               update a b c a' = 
                      if a >= 2
                      then let 
                           br = bimap (fromIntegral . round)
                           sPos = br (x + 4*cos (rad b), y + 4*sin (rad b))
                           in trace (show a') $ addBlocks c (fillBlocks b sPos 0 [])
                      else c

fillBlocks :: Float -> Point -> Float -> [Point] -> [Point]
fillBlocks b p c l = if (c <= 12.0) 
             then fillBlocks b p (c+1.0) ((S.foldr f' [] $ (s c))++l)
             else l  
             where f' = \a b -> ((roundNeigh . neigh) a) ++ b
                   s z = accepPointsAngle (z,p) (b-40) (b+40)                  

emp = empty

test = insert (0,3) Blocked $
       insert (1,3) Blocked $
       insert (1,2) Blocked $ 
       insert (1::Float,1::Float) Blocked empty

test_1 = 
       insert (-3,3) Blocked $
       insert (-2,3) Blocked $
       insert (-1,3) Blocked $
       insert (0,3) Blocked $
       insert (1,3) Blocked $
       insert (1,2) Blocked $ 
       insert (1::Float,1::Float) Blocked empty

test_2 = 
       insert (0,3) Blocked $
       insert (0,2) Blocked $
       insert (0,1) Blocked $
       insert (0,0) Blocked $
       insert (0,-1) Blocked $
       insert (0,-2) Blocked $
       insert (0::Float,-3::Float) Blocked empty


test_3 =
       insert (4,-4) Blocked $
       insert (4,-3) Blocked $
       insert (3,-3) Blocked $
       insert (2,-3) Blocked $
       insert (1,-3) Blocked $
       insert (-1,3) Blocked $ 
       insert (0,3) Blocked $
       insert (0,2) Blocked $
       insert (0,1) Blocked $
       insert (0,0) Blocked $
       insert (0,-1) Blocked $
       insert (0,-2) Blocked $
       insert (0::Float,-3::Float) Blocked empty

test_4 =
       insert (0,-1) Blocked $
       insert (0,1) Blocked $
       insert (1,-1) Blocked $
       insert (1,1) Blocked $
       insert (2,-1) Blocked $
       insert (2,1) Blocked $
       insert (3,-1) Blocked $
       insert (3,1) Blocked $
       insert (3::Float,0::Float) Blocked empty


test_5 = 
       insert (-5,0) Blocked $
       insert (-4,0) Blocked $
       insert (-3,0) Blocked $
       insert (-2,0) Blocked $
       insert (-1,0) Blocked $
       insert (-5,4) Blocked $
       insert (-4,4) Blocked $
       insert (-3,4) Blocked $
       insert (-2,4) Blocked $
       insert (-1,4) Blocked $
       insert (0,4) Blocked $
       insert (0,3) Blocked $
       insert (0,2) Blocked $
       insert (0,1) Blocked $
       insert (0::Float,0::Float) Blocked empty


test_6 = 
       insert (2,1) Blocked $
       insert (2,0) Blocked $
       insert (2,-1) Blocked $
       insert (2,-2) Blocked $
       insert (1,-2) Blocked $
       insert (0,-2) Blocked $
       insert (-1,-2) Blocked $
       insert (-2,-2) Blocked $
       insert (-2,-1) Blocked $
       insert (-2,0) Blocked $
       insert (-2,1) Blocked $
       insert (-2,2) Blocked $
       insert (-1,2) Blocked $
       insert (0,2) Blocked $
       insert (1,2) Blocked $
       insert (2::Float,2::Float) Blocked empty