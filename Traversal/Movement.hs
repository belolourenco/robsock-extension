{- 
   Description : This file lists the possible movements of an agent.
   Author : Renato Neves, Cláudio Lourenço
-}

module Traversal.Movement where

import RobSock.RobSockUtils


data Movement = MUp | MDown | MLeft | MRight | 
     MUpLeft | MUpRight | MDownLeft | MDownRight deriving (Show,Eq,Ord)
        
isDiag :: Movement -> Bool
isDiag x = if x == MUpLeft || x == MUpRight || 
          x == MDownLeft || x == MDownRight
            then True
            else False

labelDir :: Movement -> Int
labelDir x = case x of
         MUp -> 1 
         MUpRight -> 2
         MRight -> 3
         MDownRight -> 4
         MDown -> 5
         MDownLeft -> 6
         MLeft -> 7
         MUpLeft -> 8

determineDirection :: (Movement,Movement) -> (Double,RotateDir)
determineDirection (bef,cur) = 
                   let lft = g (labelDir bef) (labelDir cur) 0
                       rgt = f (labelDir bef) (labelDir cur) 0
                   in if lft <= rgt 
                      then (lft,RotLeft)
                      else (rgt,RotRight)
                   where 
                         f x y z =
                           if x == y 
                              then z  
                              else if x == 8 
                                   then f 1 y (z+1)
                                   else f (x+1) y (z+1) 
                         g x y z =
                           if x == y 
                              then z  
                              else if x == 1 
                                   then g 8 y (z+1)
                                   else g (x-1) y (z+1) 

test = [ MDown, MRight, MDown, MDown, MRight, MRight, MRight, MRight, MUp, MLeft,
     MUp, MRight, MUp, MLeft, MLeft, MLeft, MLeft, MLeft ]

test_1 = [ MDown, MDown, MDown, MDown, MDown, MRight, MRight, MRight, MRight, MRight, 
        MUp, MUp, MUp, MUp, MUp, MLeft, MLeft, MLeft, MLeft, MLeft ]

