{- 
   Description : This file regards the definition of agent.
   Author : Renato Neves, Cláudio Lourenço
-}

module Agent where

data Agent = Agent1 | Agent2 | Agent3 | Agent4 | Agent5 deriving (Eq,Ord,Show)