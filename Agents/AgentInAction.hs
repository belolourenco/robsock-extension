{- 
   Description : This file regards states and actions of agents.
   Author : Renato Neves, Cláudio Lourenço
-}

module AgentInAction where

-- imports the definition of agent
import Agent

-- Each agent is in a position in the euclidean plane. 
-- However we may not always know that position.
agentPosition :: Agent -> Maybe Point 
