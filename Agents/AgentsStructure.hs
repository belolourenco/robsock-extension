{- 
   Description : This file regards the structure of agents.
   Author : Renato Neves, Cláudio Lourenço
-}

module AgentsStructure where

-- imports the definition of agent
import Agent

-- imports the structure that we use to create the structure of agents
import Data.Set

-- the structure of agents
type AgentsStruct = Set Agent 
